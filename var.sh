export commit_date=$(git show -s --format=%ct | xargs -I{} date -d @{} +%y%m%d)
export commit_sha=${CI_COMMIT_SHORT_SHA}
export x=$commit_date-$commit_sha
echo $x